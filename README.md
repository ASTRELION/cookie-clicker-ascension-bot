# [Cookie Clicker](https://orteil.dashnet.org/cookieclicker/) Ascension Bot

Script for the [Endless Cycle achievement](https://cookieclicker.fandom.com/wiki/Ascension#Times_Ascended_and_Cookies_Sacrificed).

## Requirements

- `Python3`: https://python.org
- `Firefox (Gecko) Driver`: https://github.com/mozilla/geckodriver

## Usage

Run `python ascend.py [flags]`

## Options

Display options with `python ascend.py help`  

| Option | Short | Description |
|--------|-------|-------------|
| --url | -u | Cookie Clicker URL, defaults to https://orteil.dashnet.org/cookieclicker/ |
| --ascensions | -a | Number of ascensions to run for, defaults to 1000 |
| --file | -f | Name of save file to load, defaults to save.txt |
| --interval | -i | Save interval in number of ascensions, defaults to 10 |