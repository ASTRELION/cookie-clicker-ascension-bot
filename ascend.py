from pathlib import Path
from time import sleep
import argparse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--url", help="Cookie Clicker URL, defaults to https://orteil.dashnet.org/cookieclicker/", default="https://orteil.dashnet.org/cookieclicker/")
parser.add_argument("-a", "--ascensions", help="Number of ascensions to run for, defaults to 1000", type=int, default=1000)
parser.add_argument("-f", "--file", help="Name of save file to load, defaults to save.txt", default="save.txt")
parser.add_argument("-i", "--interval", help="Save interval in number of ascensions, defaults to 10", type=int, default=10)
args = parser.parse_args()

driver = webdriver.Firefox()
wait = WebDriverWait(driver, 10)
driver.get(args.url)

def save():

    driver.find_element(By.ID, "prefsButton").click()
    saveToFile = wait.until(EC.element_to_be_clickable((By.LINK_TEXT, "Save to file")))
    saveToFile.click()
    driver.find_element(By.ID, "prefsButton").click()

# ignore cookie banner
sleep(2)
driver.find_element(By.CLASS_NAME, "cc_btn_accept_all").click()

# select localization
driver.find_element(By.ID, "langSelect-EN").click()

# load save data
filePath = str(Path(args.file).absolute())
if (Path(filePath).is_file()):

    # select preferences
    sleep(2)
    driver.find_element(By.ID, "prefsButton").click()
    driver.find_element(By.ID, "FileLoadInput").send_keys(filePath)
    driver.find_element(By.ID, "prefsButton").click()

ascensions = 0
while (ascensions < args.ascensions):

    if ((ascensions > 0 and ascensions % args.interval == 0)):

        save()

    # while chips < 1, do stuff
    store = driver.find_element(By.ID, "store")
    ascendNumber = driver.find_element(By.ID, "ascendNumber")
    while (not ascendNumber.text or int(ascendNumber.text.replace(",", "")) < 1):

        # while upgrades, click em
        try: # try buying vault items

            vault = store.find_element(By.ID, "vaultUpgrades")
            vault.find_element(By.ID, "upgrade0").click()

        except:
            pass

        try: # try buying with buy-all

            store.find_element(By.ID, "storeBuyAllButton").click()

        except:

            try: # buy individually, instead

                upgrades = store.find_element(By.ID, "upgrades")
                upgrades.find_element(By.CLASS_NAME, "enabled").click()

            except:
                pass

        # while buildings, buy em
        try:
        
            store.find_element(By.ID, "storeBulkBuy").click()
            store.find_element(By.ID, "storeBulk100").click()
            products = store.find_element(By.ID, "products")
            products.find_element(By.CLASS_NAME, "enabled").click()

        except:
            pass

        # otherwise, click the cookie!
        driver.find_element(By.ID, "bigCookie").click()

    # Ascend!
    driver.find_element(By.ID, "legacyButton").click()
    driver.find_element(By.ID, "promptOption0").click()
    ascensions += 1
    ascendButton = wait.until(EC.element_to_be_clickable((By.ID, "ascendButton")))
    ascendButton.click()
    driver.find_element(By.ID, "promptOption0").click()

save()
print("Ascended {} times!".format(ascensions))
input("Press ENTER to close browser window (unsaved progress will be lost!)")
driver.close()